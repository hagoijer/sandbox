var sys     =  require('sys'),
    http    =  require('http'),
    url     =  require('url'),
    path    =  require('path'),
    fs      =  require('fs'),
    events  =  require('events');

function load_static_file( uri, response ) {
  var filename = path.join( process.cwd(), uri );

  fs.exists( filename, function( exists ) {
    
    if( !exists ) {
      response.setHeader('Content-Type', 'text/plain');
      response.statusCode = 404;
      response.write('404 Not Found\n');
      response.end();
      return;
    }

    fs.readFile( filename, 'binary', function( err, file ) {
      
      if( err ) {
        response.setHeader( 'Content-Type', 'text/plain' );
        response.statusCode = 500;
        response.end("Error 500");
        return;
      }

      response.statusCode = 200;
      response.write( file, 'binary' );
      response.end();

    });

  });
}


var tweet_emitter  = new events.EventEmitter();

function get_tweets() {
  
  var request = http.request({ 
    method:'GET', 
    port:80, 
    path:'/1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name=HaGoijer&count=2', 
    hostname:'api.twitter.com'
  });

  request.addListener( 'response', function(response) {
    var body = '';
    
    response.addListener( 'data', function(data) {
      body += data;
    });

    response.addListener( 'end', function() {
      var tweets = JSON.parse(body);
      if( tweets.length > 0) {
        tweet_emitter.emit('tweets', tweets);
      }
    });

  });

  request.end();
}

setInterval( get_tweets, 5000 );

http.createServer(function(request, response) {
  var uri = url.parse(request.url).pathname;
  if(uri === '/stream') {
    
    var listener = tweet_emitter.addListener('tweets', function(tweets) {      
      response.statusCode = 200;
      response.write(JSON.stringify(tweets));
      response.end();
      clearTimeout(timeout);
    });

    var timeout = setTimeout(function(){
      response.setHeader('Content-Type', 'text/plain');
      response.write(JSON.stringify([]));
      response.end();
      tweet_emitter.removeListener(listener);
    },10000);

  } else {
    load_static_file(uri, response);
  }

}).listen(process.env.PORT, process.env.IP);  
  
console.log("Server running at: " + process.env.IP +":"+ process.env.PORT);